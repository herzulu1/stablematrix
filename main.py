
import matplotlib.pyplot as plt
import scipy.linalg as scLA
import numpy as np
import sympy as sy

import wx
import wx.grid

class MyData():
    nFilas = 0
    nColumnas = 0


# Ventana para pedir el tamaño de la matriz
class MyFrame(wx.Frame):


    def __init__(self):
        super().__init__(parent=None, title='Matrix Analisis', size = (240,180))
        panel = wx.Panel(self)

        self.indicacion = wx.StaticText(panel, -1, "Ingrese el número de filas y columnas", pos=(10, 5))
        self.indicacion = wx.StaticText(panel, -1, "de la matrix que se va a analizar", pos=(10, 25))
        self.indicacion = wx.StaticText(panel, -1, "Filas y Columnas", pos=(5, 50))
        #self.indicacion = wx.StaticText(panel, -1, "Filas", pos=(5, 75))

        self.text_ctrl_F = wx.TextCtrl(panel, pos=(25, 70))
        #self.text_ctrl_C = wx.TextCtrl(panel, pos=(70, 75))
        my_btn = wx.Button(panel, label='Crear Matrix', pos=(10, 105))
        my_btn.Bind(wx.EVT_BUTTON, self.on_press)

        self.Show()

    def on_press(self, event):
        MyData.nFilas = self.text_ctrl_F.GetValue()
        MyData.nColumnas = MyData.nFilas
        self.Close()


if __name__ == '__main__':
    app = wx.App()
    frame = MyFrame()
    app.MainLoop()

# Fin Ventana de dimensiones

print(MyData.nFilas + " - " + MyData.nColumnas)


#Ventana de la tabla para rellenar


x_counter = int(MyData.nFilas)
y_counter = int(MyData.nColumnas)


Matrix = [[0 for x in range(x_counter)] for y in range(y_counter)]
InitVector = [[0 for x in range(1)] for y in range(y_counter)]

class GridFrame(wx.Frame):
    def __init__(self):
        super().__init__(parent=None, title='Matrix Analisis', size = (530,280))
        panel = wx.Panel(self)

        # Create a wxGrid object
        self.MyGrid = wx.grid.Grid(panel, -1, size = (400,190))
        self.MyGrid.CreateGrid(x_counter, y_counter)

        self.InitVectorGrid = wx.grid.Grid(panel, -1, size=(150, 190), pos=(400, 0))
        self.InitVectorGrid.CreateGrid(x_counter, 1)


        # GRID PARA EL VECTOR DE INICIO

        self.InitVectorGrid.SetColSize(0, 35)

        for x in range(0, x_counter):
            self.InitVectorGrid.SetCellValue(x, 0, str(0))
            self.InitVectorGrid.SetColFormatFloat(x, 0, 2)

        for x in range(0, x_counter):
            InitVector[x][0] = float(self.InitVectorGrid.GetCellValue(x, 0))



        # GRID PARA LA MATRIZ

        for x in range(0, x_counter):
            for y in range(0, y_counter):
                self.MyGrid.SetColSize(x, 35)
                self.MyGrid.SetCellValue(x, y, str(0))
                self.MyGrid.SetColFormatFloat(x, y, 2)


        for x in range(0, x_counter):
            for y in range(0, y_counter):
                Matrix[x][y] = float(self.MyGrid.GetCellValue(x, y))



        self.indicacion = wx.StaticText(panel, -1, "Vector Inicio", pos=(410, 200))


        my_btn = wx.Button(panel, label='Calcular', pos=(100, 200))
        my_btn.Bind(wx.EVT_BUTTON, self.on_press)

        self.Show()

    def on_press(self, event):

        for x in range(0, x_counter):
            for y in range(0, y_counter):
                stringV = self.MyGrid.GetCellValue(x, y)
                pointValue = stringV.replace(",",".")
                Matrix[x][y] = float(pointValue)

        for x in range(0, x_counter):
            stringV = self.InitVectorGrid.GetCellValue(x, 0)
            pointValue = stringV.replace(",", ".")
            InitVector[x][0] = float(pointValue)

        self.Close()


if __name__ == '__main__':

    appGrid = wx.App()
    frameGrid = GridFrame()
    appGrid.MainLoop()


print("Matrix: ")
print(Matrix)
print("InitVector: ")
print(InitVector)

M = np.matrix(Matrix)
print("M: ")
print(M)

InitV = np.matrix(InitVector)
print("InitV: ")
print(InitV)


eigvals, eigvecs = np.linalg.eig(M)

print("Eigen Values")
print(eigvals)

print("Eigen Vectors")
print(eigvecs)

convege = False
roGlobal = 0
# estable calculus
# A :matriz de eigenvectores de la matriz estocastica
# B :vector de constantes
# E :error

# X :vector slucion
# i :numero de iteraciones
# ro = radio espectral

def gs(A,B,E):
  n=len(B) # aqui tomo el tamaño del vector  inicial
  D=np.diag(np.diag(A)) # aqui se saca la diagonal en una matrix de 2 por 2
  LD=np.tril(A) # aqui se busca la lower triangular - triengular inferior
  U=np.triu(A)-D # aqui se busca la uper triangular - triengular superior
  C=np.dot(np.linalg.inv(LD),B) # vector constante para iterar
  T=-np.dot(np.linalg.inv(LD),U) # matriz de transición segun M=-(L+D)-1 U
  [val,vec]=np.linalg.eig(T) # eigen values y vecots
  ro=max(abs(val)) # radio espectral
  if ro>=1 or ro==0.9999999999999993:
      print("No converge")
      return [[], 0, ro] # No converge
  X0=np.ones([n,1],float) # Matriz vacia
  i=1
  while True:
    X1=C+np.dot(T,X0) # iteración
    print("procesando ... " + str(ro))
    print(X1)
    if np.linalg.norm(X1-X0,np.inf)<E: #chequear el error para que no sea infinita la iteracion
        return[X1,i,ro]
    i=i+1
    X0=X1.copy() #en la matriz cero se copia el resultado y se vuelve a iterar
                    #hasta que se cumpla la condición

# A = np.array([[8,9,2],[2,7,2],[2,8,6]],float)
# B = np.array([[69],[47],[68]],float)
# X,i,ro = gs(A,B,0.0001)

######
# pasar la matriz de eugen values no lso valores originales
# de la matrix estocastica

# test local
A_stoc = np.array(eigvecs,float)
B_init = np.array(InitV,float)
#A_stoc = np.array([[4/3,-1],[1,1]],float)
#B_init = np.array([[0],[1]],float)
X,i,ro = gs(A_stoc,B_init,0.0001)
print("values finales: ")
print(X)
print(i)
print(ro)

#sobre eso del 0.7071
#https://lpsa.swarthmore.edu/MtrxVibe/MatrixAll.html

# ventana con las respuestas
# Ventana para pedir el tamaño de la matriz
class ReportFrame(wx.Frame):


    def __init__(self):
        super().__init__(parent=None, title='Matrix Analisis', size = (400,300))
        panel = wx.Panel(self)
        if ro>=1 or ro==0.9999999999999993:
            self.indicacion = wx.StaticText(panel, -1, "El Radio espectral es : "+str(ro)+" mayor-igual que 1", pos=(10, 5), size=(500, 15))
            self.indicacion = wx.StaticText(panel, -1, "NO CONVERGE", pos=(10, 25), size=(500, 30))
        else:
            self.indicacion = wx.StaticText(panel, -1, "Los eigenvalues de la matriz son : ", pos=(10, 5),size = (500,15))
            self.indicacion = wx.StaticText(panel, -1, str(eigvals), pos=(10, 25),size = (500,30))
            self.indicacion = wx.StaticText(panel, -1, "Los eigenvectors de la matriz son : ", pos=(10, 50),size = (500,20))
            self.indicacion = wx.StaticText(panel, -1, str(eigvecs), pos=(10, 70),size = (500,45))

            self.indicacion = wx.StaticText(panel, -1, "Los resultados de análisis son los siguientes: ", pos=(10, 110),size = (500,15))
            self.indicacion = wx.StaticText(panel, -1, "El vector solución es: "+str(X), pos=(10, 140),size = (500,30))
            self.indicacion = wx.StaticText(panel, -1, "El número de iteraciones fueron : "+str(i), pos=(10, 175),size = (500,20))
            self.indicacion = wx.StaticText(panel, -1, "El radio espectral es : "+str(ro), pos=(10, 190),size = (500,20))

        my_btn = wx.Button(panel, label='Cerrar App', pos=(20, 220))
        my_btn.Bind(wx.EVT_BUTTON, self.on_press)

        self.Show()

    def on_press(self, event):
        self.Close()


if __name__ == '__main__':
    appReport = wx.App()
    frameReport = ReportFrame()
    appReport.MainLoop()

# Fin Ventana de dimensiones

# commandos para instalación de paquetes
# pip install wxpython
# import matplotlib.pyplot as plt
# import scipy.linalg as scLA
# import numpy as np
# import sympy as sy